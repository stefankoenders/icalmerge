<?php

namespace iCalMerge;

/**
 * iCalMerge
 * 
 * @author Stefan Koenders
 * 
 * This class allows 2 iCal files(.ics) to be merged together.
 */

class iCalMerge
{
    private $old_ics;
    private $new_ics;
    private $old_events = array();
    private $new_events = array();
    private $merged_events = array();

    public function __construct($old_ics, $new_ics)
    {
        $this->old_ics = $this->strip_ics($old_ics);
        $this->new_ics = $this->strip_ics($new_ics);
    }

    public function __toString()
    {
        return $this->merged_events;
    }

    public function get_new_date_range()
    {
        return $this->get_event_date_range($this->new_events);
    }

    public function merge($overwrite_days = true)
    {
        $this->old_events = $this->get_events($this->old_ics);
        $this->new_events = $this->get_events($this->new_ics);

        $new_date_range = $this->get_event_date_range($this->new_events);

        if ($overwrite_days) {
            $filtered_old_events = $this->filter_events($this->old_events, $new_date_range);
            foreach ($filtered_old_events as $event) {
                $this->merged_events[] = $event;
            }
        } else {
            foreach ($this->old_events as $event) {
                $this->merged_events[] = $event;
            }
        }

        foreach ($this->new_events as $event) {
            $this->merged_events[] = $event;
        }
    }

    /**
     * Returns events as array
     * @param String $ics - The STRIPPED ics file.
     * @return Array with events
     */
    private function get_events($ics)
    {
        $result = array();
        foreach (explode('BEGIN:VEVENT', $ics) as $event) {
            if ($event !== '') {
                array_push($result, 'BEGIN:VEVENT' . $event);
            }
        }

        return $result;
    }

    /**
     * Ignore events that are in the given date range
     * @param Array $events with events outside date range
     * @param Array $date_range with start and end date
     */
    private function filter_events($events, $date_range)
    {
        $filtered_events = array();

        foreach ($events as $event) {
            $date = intval(substr(explode('DTSTART:', $event)[1], 0, 8));
            if (($date < $date_range['startdate']) || ($date > $date_range['enddate'])) {
                $filtered_events[] = $event;
            }
        }

        return $filtered_events;
    }

    /**
     * @param Array $events filled with ics events
     * @return Array with start and end date for events.
     */
    private function get_event_date_range($events)
    {
        $result['startdate'] = 999999999;
        $result['enddate'] = 0;

        foreach ($events as $event) {
            $date = substr(explode('DTSTART:', $event)[1], 0, 8);
            if ($date < $result['startdate']) {
                $result['startdate'] = intval($date);
            }
            if ($date > $result['enddate']) {
                $result['enddate'] = intval($date);
            }
        }

        return $result;
    }

    private function strip_ics($ics)
    {
        $ics = preg_replace('/BEGIN:VCALENDAR\n/', '', $ics);
        $ics = preg_replace('/PRODID:.+\n/', '', $ics);
        $ics = preg_replace('/VERSION:2.0\n/', '', $ics);
        $ics = preg_replace('/END:VCALENDAR\n/', '', $ics);

        return $ics;
    }
}
